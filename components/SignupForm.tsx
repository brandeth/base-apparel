import Image from "next/image";
import React from "react";
import Navbar from "./Navbar";

const SignupForm = () => {
  return (
    <>
      <div className="py-16">
        <Navbar className="hidden lg:block shadow-none px-20" />
        <div className="flex flex-col items-center gap-2 justify-center px-10 lg:px-32">
          {/* Heading Text */}
          <h2 className="text-center lg:text-left text-7xl mb-6 tracking-[0.75rem] uppercase flex flex-col items-center lg:items-start gap-2 w-full">
            <span className="font-light text-primary-desaturated-red">
              We're
            </span>
            <span className="font-bold">Coming</span>
            <span className="font-bold">Soon</span>
          </h2>

          {/* Description Text */}
          <p className="text-center lg:text-left md:max-w-md lg:max-w-none text-lg mb-6 text-primary-desaturated-red">
            Hello fellow shoppers! We're currently building our new fashion
            store. Add your email below to stay up-to-date with announcements
            and our launch deals.
          </p>

          {/* Email Input Field with Icon Button */}
          <div className="relative w-full max-w-md lg:max-w-none">
            <input
              type="email"
              placeholder="Email Address"
              className="pl-4 pr-12 py-2 border rounded-full w-full"
            />
            <button className="absolute inset-y-0 right-0 px-[1.4rem] text-white rounded-full light-red-to-red flex items-center justify-center">
              {/* Icon - Replace with actual icon */}
              <Image
                src="/icon-arrow.svg"
                className="w-auto"
                alt="Arrow Icon"
                width={11}
                height={11}
              />
            </button>
          </div>
        </div>
      </div>
    </>
  );
};

export default SignupForm;
