import Image from "next/image";
import React from "react";
import logo from "@/public/logo.svg";

const Navbar = ({ className }: any) => {
  return (
    <nav className={`bg-white shadow-lg ${className}`}>
      <div className="max-w-6xl mx-auto py-5 px-10">
        <div className="flex justify-between">
          <div className="flex space-x-7">
            <div>
              <a href="#" className="flex items-center py-4 px-2">
                {/* Next.js Image component for optimized images */}
                <Image
                  src={logo}
                  alt="Logo - Base Apparel"
                  className="w-full"
                  width={128}
                  height={128}
                />
              </a>
            </div>
          </div>
        </div>
      </div>
    </nav>
  );
};

export default Navbar;
