import Image from "next/image";
import heroDesktop from "@/public/hero-desktop.jpg";
import heroMobile from "@/public/hero-mobile.jpg";

const Hero = () => {
  return (
    <div className="relative w-full max-h-screen">
      {/* Hero Image */}
      <Image
        className="w-full lg:hidden object-cover"
        src={heroMobile}
        alt="Hero Image"
      />
      <Image
        className="hidden lg:block object-cover w-full h-screen"
        src={heroDesktop}
        alt="Hero Image"
      />
    </div>
  );
};

export default Hero;
