import type { Config } from "tailwindcss";

const config: Config = {
  content: [
    "./pages/**/*.{js,ts,jsx,tsx,mdx}",
    "./components/**/*.{js,ts,jsx,tsx,mdx}",
    "./app/**/*.{js,ts,jsx,tsx,mdx}",
  ],
  theme: {
    extend: {
      colors: {
        backgroundImage: {
          "light-red-to-red":
            "linear-gradient(135deg, hsl(0, 80%, 86%) 0%, hsl(0, 74%, 74%) 100%)",
        },
        primary: {
          "desaturated-red": "hsl(0, 36%, 70%)",
          "soft-red": "hsl(0, 93%, 68%)",
        },
        neutral: {
          "dark-grayish-red": "hsl(0, 6%, 24%)",
        },
        gradients: {
          "white-to-light-red":
            "linear-gradient(135deg, hsl(0, 0%, 100%) 100%, hsl(0, 100%, 98%) 100%)",
          "light-red-to-red":
            "linear-gradient(135deg, hsl(0, 80%, 86%) 0%, hsl(0, 74%, 74%) 100%)",
        },
      },
      fontFamily: {
        josefin: ["Josefin Sans", "sans-serif"],
      },
      screens: {
        mobile: "375px",
        desktop: "1440px",
      },
      fontSize: {
        body: "16px",
      },
    },
  },
  plugins: [],
};
export default config;
