import Hero from "@/components/Hero";
import SignupForm from "@/components/SignupForm";

export default function Home() {
  return (
    <main className="lg:grid grid-cols-9 max-h-screen">
      <div className="order-2 col-span-4">
        <Hero />
      </div>
      <div className="order-1 col-span-5">
        <SignupForm />
      </div>
    </main>
  );
}
